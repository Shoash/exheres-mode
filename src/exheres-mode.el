;; Copyright 2009-2011 Elias Pipping <pipping@exherbo.org>
;; Distributed under the terms of the GNU General Public License v2
;; Based in part upon 'gentoo-syntax.el', which is:
;;     Copyright 2006-2009 Gentoo Foundation

;;; Commentary:
;;
;; This file provides font-lock for exhereses and exlibs.
;;
;; It also comes with a skeleton that can be inserted through C-c C-n.
;; To that end, exheres-skeleton-realname will need to be customised.
;;
;; You might also want to copy the following piece of code to your
;; ~/.emacs which makes C-c C-p insert a patch header like the one in
;;
;;   http://www.exherbo.org/docs/exheres-for-smarties.html#applying_patches
;;
;; in diff-mode.
;;
;; (eval-after-load "diff"
;;   (progn
;;     (require 'skeleton)
;;     (define-skeleton insert-exherbo-patch-header-skeleton
;;       "Inserts the standard Exherbo header for patches"
;;       nil
;;       "Source: " exheres-skeleton-realname "\n"
;;       "Upstream: \n"
;;       "Reason: ")
;;     (require 'diff-mode)
;;     (define-key diff-mode-map "\C-c\C-p" 'insert-exherbo-patch-header-skeleton)))
;;
;;; Code:

(eval-when-compile
  (require 'cl))

(require 'custom)
(require 'font-lock)
(require 'sh-script)
(require 'skeleton)

(require 'exheres-mode-keywords)

;;; Customizations
(defgroup exheres nil
  "Major mode for editing files in exheres format."
  :link '(custom-group-link :tag "Font Lock Faces group"
                            font-lock-faces)
  :group 'languages)

(defcustom exheres-skeleton-realname
  "[ Customize `exheres-skeleton-realname`! ]"
  "Name used in the skeleton"
  :type 'string
  :group 'exheres)

;;; Skeleton
(define-skeleton insert-exheres-skeleton
  "Inserts a starting point for an exheres"
  nil
  "# Copyright " (format-time-string "%Y") " " exheres-skeleton-realname "\n"
  "# Distributed under the terms of the GNU General Public License v2\n"
  "\n"
  "SUMMARY=\"\"\n"
  "DESCRIPTION=\"\n"
  "\"\n"
  "HOMEPAGE=\"\"\n"
  "DOWNLOADS=\"\"\n"
  "\n"
  "LICENCES=\"\"\n"
  "SLOT=\"0\"\n"
  "PLATFORMS=\"\"\n"
  "MYOPTIONS=\"\"\n"
  "\n"
  "DEPENDENCIES=\"\n"
  "    build:\n"
  "    build+run:\n"
  "\"\n"
  "\n"
  "DEFAULT_SRC_CONFIGURE_PARAMS=( )\n"
  "DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( )\n"
  "DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( )\n"
  "\n")

(define-skeleton insert-exlib-skeleton
  "Inserts a starting point for an exlib"
  nil
  "# Copyright " (format-time-string "%Y") " " exheres-skeleton-realname "\n"
  "# Distributed under the terms of the GNU General Public License v2\n"
  "\n"
  "SUMMARY=\"\"\n"
  "DESCRIPTION=\"\n"
  "\"\n"
  "HOMEPAGE=\"\"\n"
  "DOWNLOADS=\"\"\n"
  "\n"
  "LICENCES=\"\"\n"
  "SLOT=\"0\"\n"
  "MYOPTIONS=\"\"\n"
  "\n"
  "DEPENDENCIES=\"\n"
  "    build:\n"
  "    build+run:\n"
  "\"\n"
  "\n"
  "DEFAULT_SRC_CONFIGURE_PARAMS=( )\n"
  "DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( )\n"
  "DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( )\n"
  "\n")

;;; Font Lock
(font-lock-add-keywords 'exheres-mode exheres-font-lock)
(font-lock-add-keywords 'exlib-mode exlib-font-lock)

;;; The actual modes
;; Enforce exactly two newlines at the end of an exheres or exlib
(defun exheres-enforce-blank-line ()
  (save-excursion
    (goto-char (point-max))
    (while (and (looking-at "^$")
                (not (eq (point-min) (point)))) ; handle empty files
      (backward-char)
      (delete-char 1))
    (newline 2)))

(defun exheres-before-save-common ()
  (delete-trailing-whitespace)
  (exheres-enforce-blank-line)
  nil)

;; Any function that is called here needs to return nil, otherwise the
;; file is presumed to be written
(defun exheres-before-save-exheres ()
  (exheres-before-save-common)
  (exheres-keep-platforms-sorted))

(defun exheres-before-save-exlib ()
  (exheres-before-save-common))

;;;###autoload
(define-derived-mode exheres-mode shell-script-mode "Exheres"
  "Major mode for editing files in exheres format."
  (add-hook 'write-contents-hooks 'exheres-before-save-exheres t t)
  (sh-set-shell "bash")
  (setq fill-column 100
        indent-tabs-mode nil
        tab-width 4))

;;;###autoload
(define-derived-mode exlib-mode shell-script-mode "Exlib"
  "Major mode for editing files in exlib format."
  (add-hook 'write-contents-hooks 'exheres-before-save-exlib t t)
  (sh-set-shell "bash")
  (setq fill-column 100
        indent-tabs-mode nil
        tab-width 4))

(defun exheres-keep-platforms-sorted ()
  (cl-flet ((string->platforms (str) ; Overwrites match data
                               (mapcar (lambda (s)
                                         (string-match "^\\([-~]?\\)\\(.*\\)" s)
                                         (cons (match-string 1 s) (match-string 2 s)))
                                       (split-string str)))
            (platforms->string (pf)
                               (mapconcat (lambda (e)
                                            (concat (car e) (cdr e))) pf " "))
            (sort-platforms (pfs)
                            (sort pfs (lambda (a b)
                                        (let ((ap (cdr a))
                                              (bp (cdr b)))
                                          (cond ((string-equal ap "*") t)
                                                ((string-equal bp "*") nil)
                                                (t (string-lessp ap bp))))))))
    (save-excursion
      (goto-char (point-min))
      (let (case-fold-search ; Be case-sensitive
            (platform-regex "^PLATFORMS=\"\\([^\"]*\\)\"$"))
        (cond
         ((not (re-search-forward platform-regex nil t))
          (warn "The variable PLATFORMS needs to be set."))
         ((re-search-forward platform-regex nil t)
          (warn "The variable PLATFORMS may not be set more than once."))
         (t (let* ((platform-string (match-string-no-properties 1))
                   (platforms (save-match-data (string->platforms platform-string)))
                   (sorted-platforms (sort-platforms platforms)))
              (replace-match (platforms->string sorted-platforms) t t nil 1))))))
    nil))

;; sh-mode already uses the following C-c C-<letter> keys: cfilorstuwx
(define-key exheres-mode-map "\C-c\C-n" 'insert-exheres-skeleton)
(define-key exlib-mode-map "\C-c\C-n" 'insert-exlib-skeleton)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.exheres-0\\'" . exheres-mode))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.exlib\\'" . exlib-mode))

(provide 'exheres-mode)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:
